# Most Active Cookie

## Problem Statement

Given a cookie log file in the following format:

```
cookie,timestamp
AtY0laUfhglK3lC7,2018-12-09T14:19:00+00:00
SAZuXPGUrfbcn5UA,2018-12-09T10:13:00+00:00
5UAVanZf6UtGyKVS,2018-12-09T07:25:00+00:00
AtY0laUfhglK3lC7,2018-12-09T06:19:00+00:00
SAZuXPGUrfbcn5UA,2018-12-08T22:03:00+00:00
4sMM2LxV07bPJzwf,2018-12-08T21:30:00+00:00
fbcn5UAVanZf6UtG,2018-12-08T09:30:00+00:00
4sMM2LxV07bPJzwf,2018-12-07T23:30:00+00:00
```

Write a command line program in your preferred language to process the log file and return the most active
cookie for a specific day. 

Please include a -f parameter for the filename to process and a -d parameter to
specify the date.

e.g. we’d execute your program like this to obtain the most active cookie for 9th Dec 2018.
\$ ./[command] -f cookie_log.csv -d 2018-12-09
And it would write to stdout:
AtY0laUfhglK3lC7
We define the most active cookie as one seen in the log the most times during a given day.

#### Assumptions:
* If multiple cookies meet that criteria, please return all of them on separate lines.
* Please only use additional libraries for testing, logging and cli-parsing. There are libraries for most
languages which make this too easy (e.g pandas) and we’d like you to show off your coding skills.
* You can assume -d parameter takes date in UTC time zone.
* You have enough memory to store the contents of the whole file.
* Cookies in the log file are sorted by timestamp (most recent occurrence is the first line of the file).
We're looking for a concise, maintainable, extendable and correct solution. We're hoping you'll deliver your
solution as production grade code and demonstrate:
* good testing practices
* knowledge of build systems, testing frameworks, etc.
* clean coding practices (meaningful names, clean abstractions, etc.)
Please use a programming language you’re very comfortable with. The next stage of the interview
will involve extending your code.


## Solution

The application is built with nodejs, the source files are organized in the folder `src` the test scripts are within the `tests` folder. 

The entry point to the application is `src/index.js` which calls the `src/services/most-active-cookie.service.js` module. 

Each of the service has a single responsibility and does exactly one thing.


The code in `src/services/most-active-cookie.service.js` first converts the csv file to array of cookies, whose structure easy to parse in the rest of the application. Then a filter is applied to fetch the cookies for a given date, and the max occurrence of the cookie for a given date is fetched.

The max occurrence algorithm maintains a map of cookies with the cookie name as the key and the count as the value. For each of the cookie read from the input array to the method, the count of the corresponding cookie key name is incremented. A variable is used to store the max count, each time the increment occurs the max count variable is checked and if the incremented value is greater or equal to the max count variable value the cookie is pushed to a array.

## Execution
* To run the application, checkout the code from the repo, or unzip the zip file to a location.
* Install [node.js](https://nodejs.org/en/)
* Navigate to the path the code is saved
* Run `npm install` to setup the dependencies 
* A shell script is provided to make invoking the nodejs application easy, the script is in the `bin` folder and can be invoked as `sh ./bin/most-active-cookie.sh -f <csv path> -d <date>` to invoke the node application which will parse the file and find the most active cookie.
* To run tests run the provided scripts i.e. `sh ./bin/run-unit-tests.sh` 
