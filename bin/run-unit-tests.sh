#!/bin/sh

BASEDIR=$(dirname $0)

cd "$BASEDIR/../" || return
CURRENT_LOCATION=$(pwd)
echo "Running unit tests from path $CURRENT_LOCATION/tests/unit"
npm run test:unit
