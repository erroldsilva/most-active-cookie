
const args = require('minimist')(process.argv.slice(2));
const { findMostActiveCookie } = require('./services/most-active-cookie.service');

const errorToMessage = require('./utils/errorMessages');

/**
 * Entry point to the most active cookie application, this module does the following
 * 1. Fetch the file path & date from the command line args.
 * 2. Invoke the method to find most active cookie
 * 3. Print the result of the method invoked
 * 4. Handle and print error if any
 * 5. Exit with 0 when no errors, with 1 incase of any errors
 */
(async () => {
  try {
    const { d: date = null, f: file = null } = args;
    if (!date || !file) {
      console.log('Pass the date "-d 2001-02-10" & file "-f <path_to_the_file>" as params to the application');
    } else {
      console.log(`Parsing file ${file} for date ${date}`);
      const mostActiveCookies = await findMostActiveCookie(file, date);
      if (mostActiveCookies.length) {
        mostActiveCookies.forEach(activeCookie =>
          console.log(`Most Active Cookie for the date ${date} is ${activeCookie.cookieValue}`));
      }
    }
    process.exitCode = 0;
  } catch (error) {
    process.exitCode = 1;
    handleError(error);
  }
})();


function handleError(error) {
  const errorMessage = errorToMessage[error.message]
  if (errorMessage) {
    console.error(`ERROR: ${errorMessage}`);
  } else {
    console.error(`CRITICAL: An error occurred while finding most active cookie; unable to proceed`, error);
  }
}
