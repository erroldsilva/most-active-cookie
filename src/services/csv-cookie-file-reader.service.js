const { readCSVFile } = require('../utils/csvFileReader');
const { checkFileExists } = require('../utils/fileValidation');

module.exports = {
  readCookieFile
}

/**
 * Reads the CSV file and returns the cookie & time stamp as CookieDetails
 * If the file cannot be found or there are no cookies in the file throws an error
 * @param {string} filePath path to the CSV file
 * @returns Promise<Cookies[]>
 */
async function readCookieFile(filePath) {
  if (checkFileExists(filePath)) {
    const fileContents = await readCSVFile(filePath);
    if (fileContents.length) {
      return fileContents.map(line => {
        return {
          cookieValue: line.cookie,
          cookieTimestamp: new Date(line.timestamp)
        }
      });
    } else {
      throw Error('NO_COOKIES');
    }
  } else {
    throw Error('FILE_NOT_FOUND');
  }
}
