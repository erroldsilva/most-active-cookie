

module.exports = {
  filterSpecificDate
}

/**
 * Returns a list of cookies for a specific day
 * @param {Cookies[]} allCookies array of cookies to be filtered
 * @param {string} compareValue the filter compare value
 * @returns Cookies[]
 */
function filterSpecificDate(allCookies, compareValue) {
  const dateSpecificCookies =  allCookies.filter(cookie =>
    cookie.cookieTimestamp.toISOString().split('T')[0] === compareValue)
  
  if (dateSpecificCookies.length) {
    return dateSpecificCookies;
  } else {
    throw Error('COOKIES_FOR_DATE_NOT_FOUND');
  }
}
