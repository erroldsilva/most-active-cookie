
module.exports = {
  findMaxOccurrence
}


/**
 * Gets the max occurrence of a cookie withing the given list of cookies
 * @param {Cookies[]} allCookies 
 * @returns Cookies[]
 */
function findMaxOccurrence(allCookies) {
  let maxOccurrence = 0;
  let maxOccurringCookies = [];
  let cookieMap = {};
  allCookies.forEach(cookie => {
    const { cookieValue: cookieMapKey} = cookie;
    if (!cookieMap[cookieMapKey]) {
      cookieMap[cookieMapKey] = 1;
    } else {
      cookieMap[cookieMapKey] += 1;
    }
    if (maxOccurrence < cookieMap[cookieMapKey]) {
      maxOccurrence = cookieMap[cookieMapKey];
      maxOccurringCookies = [cookie];
    } else if (maxOccurrence === cookieMap[cookieMapKey]) {
      maxOccurringCookies.push(cookie);
    }

  })

  return maxOccurringCookies;
}
