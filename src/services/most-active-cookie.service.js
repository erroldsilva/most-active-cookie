

const { readCookieFile } = require('./csv-cookie-file-reader.service');
const { filterSpecificDate } = require('./filter-cookie-specific-day.service');
const { findMaxOccurrence } = require('./find-max-occurrence.service');

module.exports = {
  findMostActiveCookie
}

/**
 * The composer function which accepts the input and calls the other methods to
 * 1. Read the CSV file and fetch the cookies
 * 2. Filter Cookies for a specific date
 * 3. Find the max cookie for a the specified date.
 * @param {string} pathOfFileWithCookies path of the cookies file 
 * @param {string} activeOnDate the date on which the most active cookie has to be found
 * @returns Cookies[]
 */
async function findMostActiveCookie(pathOfFileWithCookies, activeOnDate) {
  const cookies = await readCookieFile(pathOfFileWithCookies);
  const dateSpecificCookies = filterSpecificDate(cookies, activeOnDate);
  return findMaxOccurrence(dateSpecificCookies);
}
