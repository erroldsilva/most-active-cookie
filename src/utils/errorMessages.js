

module.exports = {
  FILE_NOT_FOUND: 'No file found in the path passed to the application',
  NO_COOKIES: 'No cookies found in the CSV file',
  COOKIES_FOR_DATE_NOT_FOUND: 'No cookies found for selected date'
}
