
const { accessSync } = require('fs');

module.exports = {
  checkFileExists
}

function checkFileExists(filePath) {
  let fileExists = false;
  try {
    accessSync(filePath);
    fileExists = true;
  } catch (error) {
    // nothing to do here as error need not be handled
  }
  return fileExists;
}
