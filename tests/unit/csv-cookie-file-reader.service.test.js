let csvCookieFileReader = null;
let mockCsvFileReaderUtils = null;
let mockFileValidation = null;

describe('CSV Cookie File Reader', () => {
  beforeEach(() => {
    jest.resetModules();
    jest.doMock('../../src/utils/csvFileReader.js');
    jest.doMock('../../src/utils/fileValidation.js');
    mockCsvFileReaderUtils = require('../../src/utils/csvFileReader.js');
    csvCookieFileReader = require('../../src/services/csv-cookie-file-reader.service');
    mockFileValidation = require('../../src/utils/fileValidation.js')
  });

  it('check the csv file reader returns a array of json objects', async () => {
    const mockDate = new Date();
    mockFileValidation.checkFileExists.mockReturnValue(true);
    mockCsvFileReaderUtils.readCSVFile.mockResolvedValueOnce([{ cookie: 'cookie', timestamp: mockDate }]);
    const result = await csvCookieFileReader.readCookieFile('mockFilePath');
    expect(result).toEqual([{ "cookieTimestamp": mockDate, "cookieValue": "cookie" }]);
  });

  it('throws NO_COOKIES error when the csv file is empty', async () => {
    mockFileValidation.checkFileExists.mockReturnValue(true);
    mockCsvFileReaderUtils.readCSVFile.mockResolvedValueOnce([]);
    expect(async () => {
      await csvCookieFileReader.readCookieFile('mockFilePath')
    }).rejects.toThrow('NO_COOKIES')
  });

  it('throws FILE_NOT_FOUND error when the csv file in path cannot be found', async () => {
    mockFileValidation.checkFileExists.mockReturnValue(false);
    expect(async () => {
      await csvCookieFileReader.readCookieFile('mockFilePath')
    }).rejects.toThrow('FILE_NOT_FOUND')
  });

});
