let filterCookieSpecificDay = null;


describe('Filter Cookie Specific Day', () => {
  beforeEach(() => {
    jest.resetModules();
    filterCookieSpecificDay = require('../../src/services/filter-cookie-specific-day.service');
  });

  it('filters cookies for a specific date', () => {
    const mockDate = new Date();
    const mockInput = [{ "cookieTimestamp": mockDate, "cookieValue": "cookie" },
    { "cookieTimestamp": new Date(`2018-12-09T14:19:00+00:00`), "cookieValue": "cookie" }];
    const result = filterCookieSpecificDay.filterSpecificDate(mockInput, mockDate.toISOString().split('T')[0]);
    expect(result).toEqual([mockInput[0]]);
  });

  it('throws COOKIES_FOR_DATE_NOT_FOUND error when no cookie is found for a specific date', () => {
    const mockDate = new Date();
    const mockInput = [{ "cookieTimestamp": new Date(`2018-12-09T14:19:00+00:00`), "cookieValue": "cookie" }];
    expect(() => {
      filterCookieSpecificDay.filterSpecificDate(mockInput, mockDate.toISOString().split('T')[0])
    }).toThrow('COOKIES_FOR_DATE_NOT_FOUND')
  });


});
