let findMaxOccurrence = null;


describe('Find Max Occurrence', () => {
  beforeEach(() => {
    jest.resetModules();
    ({ findMaxOccurrence } = require('../../src/services/find-max-occurrence.service'));
  });

  it('returns cookie with the most occurrence when there are multiple cookies with the same name', () => {
    const mockDate = new Date();
    const mockInput = [{ "cookieTimestamp": mockDate, "cookieValue": "cookie" },
    { "cookieTimestamp": mockDate, "cookieValue": "cookie" },
    { "cookieTimestamp": mockDate, "cookieValue": "cookie" },
    { "cookieTimestamp": new Date(`2018-12-09T14:19:00+00:00`), "cookieValue": "cookie2" }];
    const result = findMaxOccurrence(mockInput, mockDate.toISOString().split('T')[0]);
    expect(result).toEqual([mockInput[0]]);
  });

  it('returns multiple cookies with the most occurrence when there are multiple cookies with same count', () => {
    const mockDate = new Date();
    const mockInput = [{ "cookieTimestamp": mockDate, "cookieValue": "cookie" },
    { "cookieTimestamp": mockDate, "cookieValue": "cookie2" }];
    const result = findMaxOccurrence(mockInput, mockDate.toISOString().split('T')[0]);
    expect(result).toEqual(mockInput);
  });

});
