
let readCookieFile = null;
let filterSpecificDate = null;
let findMaxOccurrence = null;
let findMostActiveCookie = null;

describe('Most Active Cookie', () => {

  beforeEach(() => {
    jest.resetModules();
    jest.mock('../../src/services/csv-cookie-file-reader.service.js');
    jest.mock('../../src/services/filter-cookie-specific-day.service.js');
    jest.mock('../../src/services/find-max-occurrence.service.js');
    ({ readCookieFile } = require('../../src/services/csv-cookie-file-reader.service'));
    ({ filterSpecificDate } = require('../../src/services/filter-cookie-specific-day.service'));
    ({ findMaxOccurrence } = require('../../src/services/find-max-occurrence.service'));
    ({ findMostActiveCookie } = require('../../src/services/most-active-cookie.service'));
  });


  it('returns the most active cookie when none of the functions throw an error', async () => {
    const mockDate = new Date();
    const mockResult = [{ "cookieTimestamp": mockDate, "cookieValue": "cookie" }];
    readCookieFile.mockResolvedValue(mockResult);
    filterSpecificDate.mockResolvedValue(mockResult);
    findMaxOccurrence.mockResolvedValue(mockResult);
    const result = await findMostActiveCookie('mockFilePath', mockDate.toISOString().split('T')[0]);
    expect(result).toEqual(mockResult);
  });

  it('returns the error throws as is', () => {
    const mockDate = new Date();
    readCookieFile.mockRejectedValue(new Error('MOCK_ERROR'));
    expect(async () => {
      await findMostActiveCookie('mockFilePath', mockDate.toISOString().split('T')[0]);
    })
      .rejects
      .toThrow('MOCK_ERROR');
  });

});
